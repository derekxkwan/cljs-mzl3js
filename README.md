# cljs-mzl3js

an interpretation of the [Mozilla Three.js demo](https://developer.mozilla.org/en-US/docs/Games/Techniques/3D_on_the_web/Building_up_a_basic_demo_with_Three.js?utm_campaign=feed&utm_medium=rss&utm_source=developer.mozilla.org) with CLojurescript

## Usage

FIXME

## License


Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
