(defproject cljs-mzl3js "0.0.1"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.9.229"]
                 [cljsjs/three "0.0.91-0"]]
  :plugins [[lein-cljsbuild "1.1.7"]
            [lein-figwheel "0.5.7"]]
  :clean-targets ^{:protect false} [:target-path "out" "resources/public/cljs"]
  :cljsbuild {
    :builds [{:id "dev"
              :source-paths ["src"]
              :figwheel true
              :compiler {:main "cljs-mzl3js.core"
                         :asset-path "cljs/out"
                         :output-to "resources/public/cljs/main.js"
                         :output-dir "resources/public/cljs/out"
                         }

             }]
   }
   :figwheel {
     :ansi-color-output false
     :css-dirs ["resources/public/css"]
   }
  :repl-options {:color false}
)
