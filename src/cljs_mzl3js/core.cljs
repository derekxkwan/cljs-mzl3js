(ns cljs-mzl3js.core
  (:require [cljsjs.three]
            [cljs-mzl3js.three-view :as view]
            [cljs-mzl3js.three-vec :as vec]))

(def view-angle 70)
(def view-near 0.1)
(def view-far 1000)
(def THREE js/THREE)

(def c-elt (.getElementById js/document "mainc"))
(set! (.-width c-elt) (.-innerWidth js/window))
(set! (.-height c-elt) (.-innerHeight js/window))
(def WIDTH (.-width c-elt))
(def HEIGHT (.-height c-elt))

(def scene (view/scene))

(def camera (view/perspec-camera WIDTH HEIGHT [0 0 50] :angle view-angle :near view-near :far view-far))

(def renderer (view/renderer c-elt WIDTH HEIGHT (.-devicePixelRatio js/window) :clear-color 0xdddddd))


(def my-cube
  (let [geom (THREE.BoxGeometry. 10 10 10)
        mat (THREE.MeshBasicMaterial. #js {:color 0x0095DD})
        cube (THREE.Mesh. geom mat)]
    cube))

(view/rotation-set! my-cube [0.4 0.2 0])
(view/position-set! my-cube [-25 0 0])


;;(view/add-point-light! scene [0 0 10])

(def my-torus
  (let [geom (THREE.TorusGeometry. 7 1 6 12)
        mat (THREE.MeshPhongMaterial. #js {:color 0xFF9500})
        torus (THREE.Mesh. geom mat)]
    torus))




(def my-dodeca
  (let [geom (THREE.DodecahedronGeometry. 7)
        mat (THREE.MeshLambertMaterial. #js {:color 0xEAEFF2})
        dodeca (THREE.Mesh. geom mat)]
    dodeca))

(doall (map #(view/scene-add! scene %) [my-cube my-torus my-dodeca]))

(view/position-set! my-dodeca [25 0 0])

(def my-pt-light (THREE.PointLight. 0xffffff))

(view/position-set! my-pt-light [-10 15 50])

(view/scene-add! scene my-pt-light)

(defn updatefn []
  (let [cub-rot (view/rotation-get my-cube)
        torus-rot (view/rotation-get my-torus)
        dodeca-rot (view/rotation-get my-dodeca)]
    (view/rotation-set! my-cube (vec/add-vec [0.01 0.02 0] cub-rot))
    (view/rotation-set! my-torus (vec/add-vec [0 0.01 0.02] torus-rot))
    (view/rotation-set! my-dodeca (vec/add-vec [0.02 0 0.01] dodeca-rot))
    (set! (.-verticesNeedUpdating (.-geometry my-cube)) true)
    (set! (.-normalsNeedUpdating (.-geometry my-cube)) true)
    )
  )

(defn render []
  (.requestAnimationFrame js/window render)
  (updatefn)
  (.render renderer scene camera))

(render)
